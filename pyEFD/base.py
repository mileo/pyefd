
from datetime import date
import locale

separador = u'|'
reg_inicial = u'001'
reg_final   = u'990'
col_count = 'QTD_LIN_'


class excecao_base(Exception):
    
    def __init__(self, mensagem, registro = None, coluna = None, num_linha=0):
        
        super(excecao_base, self).__init__()
        
        self.mensagem = mensagem
        self.registro = registro
        self.coluna = coluna
        self.num_linha = num_linha
    
    def __str__(self):
        
        resultado = ''
        
        if self.registro == None:
            resultado = self.mensagem
        else:
            if self.coluna == None:
                resultado = self.registro.reg + ': ' + self.mensagem
            else:
                resultado = self.registro.reg + '-' + self.coluna.nome + ': ' + self.mensagem 
        
        if self.num_linha>0:
            resultado = '[' + str(self.num_linha) + '] ' + resultado
            
        return resultado

class excecoes_base(object):

    def __init__(self, container=None):
        if container is None:
            container = []
        self.container = container
        
        self.num_linha = 0
        
    def __str__(self):
        return str(self.container)

    def __repr__(self):
        return str(self.container)

    def __getitem__(self, key):
        return self.container[key]

    def __len__(self):
        return len(self.container)
    
    def __iadd__(self, new_excecoes):
        
        self.container += new_excecoes.container
        
        return self
    
    def add(self, componente):
        
        self.container.append(componente)
    
    def inicia(self):
        
        self.container = []
        self.num_linha = 0
        
class componente_base(object):
    
    def __init__(self, componentes = None):
        
        if componentes == None:
            componentes = componentes_base()
        
        self.componentes = componentes
    
    def add(self, componente):
        self.componentes.add(componente)
        
    def para_arquivo_interno(self, linhas):
        pass
        
    def para_arquivo(self):
        
        linhas = []
        
        linhas += self.para_arquivo_interno()
        
        for componente in self.componentes:

            linhas += componente.para_arquivo()
        
        return linhas
    
    def contador_interno(self):
        pass
    
    def contador(self):

        conta = 0
        
        conta += self.contador_interno()
        
        for componente in self.componentes:
                        
            conta += componente.contador()
        
        return conta
    
    def conta_registro_interno(self, acumulador):
        pass
    
    def conta_registro(self, acumulador):
        
        self.conta_registro_interno(acumulador)

        for componente in self.componentes:
            
            componente.conta_registro(acumulador)
    
    def valida_registro_interno(self, excecoes):
        
        pass

    def valida_registro(self, excecoes):
        
        self.valida_registro_interno(excecoes)
        
        for componente in self.componentes:
            
            componente.valida_registro(excecoes)
        
class componentes_base(object):

    def __init__(self, container=None):
        if container is None:
            container = []
        self.container = container
        
    def __str__(self):
        return str(self.container)

    def __repr__(self):
        return str(self.container)

    def __getitem__(self, key):
        return self.container[key]

    def __len__(self):
        return len(self.container)
    
    def __iadd__(self, new_registros):
        
        self.container += new_registros.container
        
        return self
    
    def add(self, componente):
        
        self.container.append(componente)
        
        
class coluna_base(object):

    def __init__(self, nome, valor=None, obrigatorio=False, valores=[], decimais=2):
        self.nome = nome
        self.valor = valor
        self.obrigatorio = obrigatorio
        self.valores = valores
        self.decimais = decimais

    def tipo(self):

        return type(self.valor)
    
    def para_arquivo_data(self):

        return "%02d%02d%04d" % (self.valor.day, self.valor.month, self.valor.year)

    def para_arquivo_float(self):

        locale.setlocale(locale.LC_ALL, "")
        
        formato = u'%.' + str(self.decimais) + 'f'

        return locale.format(formato, self.valor)        

    def para_arquivo(self):
        
        x = self.tipo()
        
        if x == date:
            
            return self.para_arquivo_data()
        
        elif x == float:
        
            return self.para_arquivo_float()
        
        else:

            if self.valor == None:
                return u''
            else:
                return str(self.valor)
    
    def vazio_data(self):
        
        return False
    
    def vazio_float(self):
        
        if self.valor == None:
            return True
        else:
            return False
        
    def vazio(self):
        
        x = self.tipo()
        
        if x == date:
            
            return self.vazio_data()
        
        elif x == float:
        
            return self.vazio_float()
        
        else:

            if self.valor == None or str(self.valor) == '':
                return True
            else:
                return False
            
            
    def valida(self):
        
        erros = []
        
        vazio = self.vazio()
        
        if self.obrigatorio and vazio:
            
            erros.append('Campo obrigatorio nao foi preenchido')
        
        valor_arquivo = self.para_arquivo()
        
        if not vazio and self.valores != [] and not valor_arquivo in self.valores:
            
            erros.append('Valor invalido: ' + valor_arquivo + \
                         ' - Valores possiveis' + str(self.valores))
        
        return erros
            


class colunas_base(object):
    def __init__(self, container=None):
        if container is None:
            container = []
        self.container = container

    def add_coluna(self, coluna_base):
        self.container.append(coluna_base)

    def add(self, nome, valor):
        coluna = coluna_base(nome, valor)
        self.add_coluna(coluna)
        
    def __str__(self):
        return str(self.container)

    def __repr__(self):
        return str(self.container)

    def __getitem__(self, key):
        return self.container[key]

    def __len__(self):
        return len(self.container)


class registro_base(componente_base):

    def __init__(self, reg, componentes = None, colunas = None):
        
        super(registro_base, self).__init__(componentes)
        
        self.reg = reg
        
        if colunas is None:
            colunas = colunas_base()
            
        self.colunas = colunas


    def add_coluna(self, nome, valor):
        
        self.colunas.add(nome, valor)

    def para_arquivo_interno(self):
        
        linhas = []
        
        linha = separador + self.reg + separador
        
        for coluna in self.colunas:
            linha += coluna.para_arquivo() + separador
            
        linhas.append(linha)
        
        return linhas
    
    def contador_interno(self):
        
        return 1
    
    def conta_registro_interno(self, acumulador):
        
        if acumulador.has_key(self.reg):
            acumulador[self.reg] += 1
        else:
            acumulador.update({self.reg : 1})

    def valida_registro_interno(self, excecoes):
        
        excecoes.num_linha += 1
        
        for coluna in self.colunas:
    
            mensagens = coluna.valida()
        
            for mensagem in mensagens:
                
                excecao = excecao_base(mensagem=mensagem, \
                                       registro=self, \
                                       coluna=coluna, \
                                       num_linha = excecoes.num_linha)
                excecoes.add(excecao)

    def chave(self):
        return u''
    
class registros_base(componente_base):
    def __init__(self, componentes=None, gera_indice = False):
        
        super(registros_base, self).__init__(componentes)
        
        self.container = []
        
        self.gera_indice = gera_indice
        
        self.indice = {}
        
    def __str__(self):
        return str(self.container)

    def __repr__(self):
        return str(self.container)

    def __getitem__(self, key):
        return self.container[key]

    def __len__(self):
        return len(self.container)
    
    def __iadd__(self, new_registros):
        
        self.container += new_registros.container
        
        return self

    def add_registro(self, registro):

        self.container.append(registro)
        
        if self.gera_indice:
            
            if not self.indice.has_key(registro.chave()):

                self.indice[registro.chave()] = len(self.container) - 1
            
    
    def check_registro(self, registro):
        
        resultado = False
        
        if self.gera_indice:

            # Insere somente se nao ha registro equivalente            
            if not self.indice.has_key(registro.chave()):
                
                self.add_registro(registro)
                
                resultado = True
            
        return resultado
    
    def registro_por_chave(self, chave):
        
        indice = self.indice.get(chave, -1)
        
        if indice >= 0:
            
            return self[indice]
        
        else:
            
            return None

    def para_arquivo_interno(self):

        linhas = []
        
        for registro in self.container:
            
            linhas += registro.para_arquivo()
        
        return linhas

    def contador_interno(self):
        
        conta = 0
        
        for registro in self.container:
            
            conta += registro.contador()
        
        return conta

    def conta_registro_interno(self, acumulador):
        
        for registro in self.container:
        
            registro.conta_registro(acumulador)

    def valida_registro_interno(self, excecoes):

        for registro in self.container:
        
            registro.valida_registro(excecoes)
            
    
class bloco_base(registros_base):
    nome = u''
    
    def __init__(self, nome, container = None):
        
        super(bloco_base, self).__init__(container)
        
        self.nome = nome
    
    def pega_registro_ini(self):

        nome_reg_ini = self.nome + reg_inicial
        
        registro_ini = registro_base(nome_reg_ini)
        
        if len(self.container) == 0:
            ind_mov = '1'
        else:
            ind_mov = '0'
        
        registro_ini.add_coluna('IND_MOV', ind_mov)
        
        return registro_ini
    
    def contador_interno(self):
        
        return super(bloco_base, self).contador_interno() + 2

    def conta_registro_interno(self, acumulador):
        
        registro_ini = self.pega_registro_ini()
        registro_ini.conta_registro(acumulador)

        super(bloco_base, self).conta_registro_interno(acumulador)
        
        registro_fim = self.pega_registro_fim()
        registro_fim.conta_registro(acumulador)
            
    def pega_registro_fim(self):
        
        nome_reg_fim = self.nome + reg_final
        
        registro_fim = registro_base(nome_reg_fim)
        
        nome_coluna = col_count + self.nome
        
        registro_fim.add_coluna(nome_coluna, self.contador())
        
        return registro_fim
        
    def para_arquivo(self):
        
        linhas = []

        registro_ini = self.pega_registro_ini()
        linhas += registro_ini.para_arquivo()
        
        linhas += super(bloco_base, self).para_arquivo()
        
        registro_fim = self.pega_registro_fim()
        linhas += registro_fim.para_arquivo()
        
        return linhas

    def valida_registro(self, excecoes):
        
        registro_ini = self.pega_registro_ini()
        registro_ini.valida_registro(excecoes)
        
        super(bloco_base, self).valida_registro(excecoes)
        
        registro_fim = self.pega_registro_fim()
        registro_fim.valida_registro(excecoes)

    
class efd_base(object):
    
    def __init__(self, registros = None):
        
        
        if registros == None:
            registros = registros_base()
        
        self.registros = registros

        self.registro_0000 = self.gera_registro_0000()

        self.registros.add(self.registro_0000)
    
    def gera_bloco_9(self):
        
        bloco_9 = bloco_base('9')
        
        acumulador = {}
        
        for componente in self.registro_0000.componentes:
            componente.conta_registro(acumulador)
            
        registros = sorted(acumulador.items())
            
        # Contador para 0000
        novo_registro = registro_base('9900')
        novo_registro.add_coluna('REG_BLC', '0000')
        novo_registro.add_coluna('QTD_REG_BLC', 1)
        bloco_9.add_registro(novo_registro)

        for linha in registros:
            novo_registro = registro_base('9900')
            novo_registro.add_coluna('REG_BLC', linha[0])
            novo_registro.add_coluna('QTD_REG_BLC', linha[1])
            bloco_9.add_registro(novo_registro)
        
        # Adiciona contador para os registros do bloco 9
       
        # Contador para 9001
        novo_registro = registro_base('9900')
        novo_registro.add_coluna('REG_BLC', '9001')
        novo_registro.add_coluna('QTD_REG_BLC', 1)
        bloco_9.add_registro(novo_registro)

        # Contador para 9990
        novo_registro = registro_base('9900')
        novo_registro.add_coluna('REG_BLC', '9990')
        novo_registro.add_coluna('QTD_REG_BLC', 1)
        bloco_9.add_registro(novo_registro)

        # Contador para 9999
        novo_registro = registro_base('9900')
        novo_registro.add_coluna('REG_BLC', '9999')
        novo_registro.add_coluna('QTD_REG_BLC', 1)
        bloco_9.add_registro(novo_registro)
        
        # Contador para 9990
        novo_registro = registro_base('9900')
        novo_registro.add_coluna('REG_BLC', '9900')
        novo_registro.add_coluna('QTD_REG_BLC', len(bloco_9.container) + 1)
        bloco_9.add_registro(novo_registro)
        
        return bloco_9
    
    
    def gera_registro_0000(self):
        
        registro_0000 = registro_base('0000')
       
        return registro_0000
    
    def pega_registros(self):
        
        registros_full = registros_base()
        
        # Clona registro 0000
        registro_0000 = registro_base(self.registro_0000.reg)
        
        for coluna in self.registro_0000.colunas:
            registro_0000.add_coluna(coluna.nome, coluna.valor)
        
        for componente in self.registro_0000.componentes:
            registro_0000.componentes.add(componente)
        
        # Gera bloco 9
        registro_0000.componentes.add(self.gera_bloco_9())
        
        registros_full.add_registro(registro_0000)
        
        # Adiciona registro 9999
        registro_9999 = registro_base('9999')
        count_efd = registros_full.contador()
        registro_9999.add_coluna('QTD_LIN', count_efd + 1)
        registros_full.add_registro(registro_9999)
        
        return registros_full
    
    def para_arquivo(self):
        
        linhas = []
        
        registros_full = self.pega_registros()
        
        for registro in registros_full:
            linhas+= registro.para_arquivo()
        
        return linhas
    
    def cria_excecoes(self):
        
        return excecoes_base()
    
    def valida(self):
        
        excecoes = self.cria_excecoes()
        
        registros_full = self.pega_registros()
        
        for registro in registros_full:
            self.registro_0000.valida_registro(excecoes)
        
        return excecoes
    
    def valida_para_arquivo(self):

        linhas = []
        excecoes = self.cria_excecoes()
        
        registros_full = self.pega_registros()
        
        for registro in registros_full:
            linhas+= registro.para_arquivo()
            registro.valida_registro(excecoes)
        
        return [linhas, excecoes]
            