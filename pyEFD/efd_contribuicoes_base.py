# -*- encoding: utf-8 -*-

from base import *
from datetime import date

# --------------------------------------------------------------------------------------------------#
# -------------------- CLASSES BASICAS EFD CONTRIBUICOES -------------------------------------------#
# --------------------------------------------------------------------------------------------------#
class excecao_efd_contribuicoes_base(excecao_base):
	pass

class coluna_efd_contribuicoes_base(coluna_base):
	pass

class registro_efd_contribuicoes_base(registro_base):
	pass

class registros_efd_contribuicoes_base(registros_base):
	pass

class bloco_efd_contribuicoes_base(bloco_base):
	pass

class efd_contribuicoes_base(efd_base):
	pass

# --------------------------------------------------------------------------------------------------#
# ----------------------------------------- EXCECOES -----------------------------------------------#
# --------------------------------------------------------------------------------------------------#

class excecao_entrada_invalida(excecao_efd_contribuicoes_base):
	pass
# --------------------------------------------------------------------------------------------------#
# ----------------------------------------- REGISTROS ----------------------------------------------#
# --------------------------------------------------------------------------------------------------#

#REGISTRO 0000: ABERTURA DO ARQUIVO DIGITAL E IDENTIFICAÇÃO DA PESSOA JURÍDICA

class registro_0000(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_0000, self).__init__('0000')

		# Colunas do registro 0000		
		self.COD_VER          = coluna_efd_contribuicoes_base('COD_VER', None, True, ['002', '003'])
		self.TIPO_ESCRIT      = coluna_efd_contribuicoes_base('TIPO_ESCRIT', None, True, ['0', '1'])
		self.IND_SIT_ESP      = coluna_efd_contribuicoes_base('IND_SIT_ESP', None)
		self.NUM_REC_ANTERIOR = coluna_efd_contribuicoes_base('NUM_REC_ANTERIOR', None)
		self.DT_INI           = coluna_efd_contribuicoes_base('DT_INI', None, True)
		self.DT_FIN           = coluna_efd_contribuicoes_base('DT_FIN', None, True)
		self.NOME             = coluna_efd_contribuicoes_base('NOME', None, True)
		self.CNPJ             = coluna_efd_contribuicoes_base('CNPJ', None, True)
		self.UF               = coluna_efd_contribuicoes_base('UF', None, True)
		self.COD_MUN          = coluna_efd_contribuicoes_base('COD_MUN', None, True)
		self.SUFRAMA          = coluna_efd_contribuicoes_base('SUFRAMA', None)
		self.IND_NAT_PJ       = coluna_efd_contribuicoes_base('IND_NAT_PJ', None, ['00', '01', '02'])
		self.IND_ATIV         = coluna_efd_contribuicoes_base('IND_ATIV', None, True, ['0', '1', '2', '3', '4', '9'])
		
		self.colunas.add_coluna(self.COD_VER)
		self.colunas.add_coluna(self.TIPO_ESCRIT)
		self.colunas.add_coluna(self.IND_SIT_ESP)
		self.colunas.add_coluna(self.NUM_REC_ANTERIOR)
		self.colunas.add_coluna(self.DT_INI)
		self.colunas.add_coluna(self.DT_FIN)
		self.colunas.add_coluna(self.NOME)
		self.colunas.add_coluna(self.CNPJ)
		self.colunas.add_coluna(self.UF)
		self.colunas.add_coluna(self.COD_MUN)
		self.colunas.add_coluna(self.SUFRAMA)
		self.colunas.add_coluna(self.IND_NAT_PJ)
		self.colunas.add_coluna(self.IND_ATIV)
		
		# Bloco 0
		self.bloco_0 = bloco_0(self)
		self.componentes.add(self.bloco_0)
		
		# Bloco A
		self.bloco_A = bloco_A(self)
		self.componentes.add(self.bloco_A)
		
		# Bloco C
		self.bloco_C = bloco_C(self)
		self.componentes.add(self.bloco_C)
		
		# Bloco D
		self.bloco_D = bloco_D(self)
		self.componentes.add(self.bloco_D)
		
		# Bloco F
		self.bloco_F = bloco_F(self)
		self.componentes.add(self.bloco_F)
		
# REGISTRO 0100: DADOS DO CONTABILISTA
class registro_0100(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_0100, self).__init__('0100')

		# Colunas	
		self.NOME = coluna_efd_contribuicoes_base('NOME', None, True)
		self.CPF = coluna_efd_contribuicoes_base('CPF', None, True)
		self.CRC = coluna_efd_contribuicoes_base('CRC', None, True)
		self.CNPJ = coluna_efd_contribuicoes_base('CNPJ', None)
		self.CEP = coluna_efd_contribuicoes_base('CEP', None)
		self.END = coluna_efd_contribuicoes_base('END', None)
		self.NUM = coluna_efd_contribuicoes_base('NUM', None)
		self.COMPL = coluna_efd_contribuicoes_base('COMPL', None)
		self.BAIRRO = coluna_efd_contribuicoes_base('BAIRRO', None)
		self.FONE = coluna_efd_contribuicoes_base('FONE', None)
		self.FAX = coluna_efd_contribuicoes_base('FAX', None)
		self.EMAIL = coluna_efd_contribuicoes_base('EMAIL', None)
		self.COD_MUN = coluna_efd_contribuicoes_base('COD_MUN', None)

		self.colunas.add_coluna(self.NOME)
		self.colunas.add_coluna(self.CPF)
		self.colunas.add_coluna(self.CRC)
		self.colunas.add_coluna(self.CNPJ)
		self.colunas.add_coluna(self.CEP)
		self.colunas.add_coluna(self.END)
		self.colunas.add_coluna(self.NUM)
		self.colunas.add_coluna(self.COMPL)
		self.colunas.add_coluna(self.BAIRRO)
		self.colunas.add_coluna(self.FONE)
		self.colunas.add_coluna(self.FAX)
		self.colunas.add_coluna(self.EMAIL)
		self.colunas.add_coluna(self.COD_MUN)

# REGISTRO 0110: REGIMES DE APURAÇÃO DA CONTRIBUIÇÃO SOCIAL E DE APROPRIAÇÃO DE CRÉDITO
class registro_0110(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_0110, self).__init__('0110')
		self.COD_INC_TRIB  = coluna_efd_contribuicoes_base('COD_INC_TRIB', None, True, ['1', '2', '3'])
		self.IND_APRO_CRED = coluna_efd_contribuicoes_base('IND_APRO_CRED', None, False, ['1', '2'])
		self.COD_TIPO_CONT = coluna_efd_contribuicoes_base('COD_TIPO_CONT', None, False, ['1', '2'])
		self.IND_REG_CUM   = coluna_efd_contribuicoes_base('IND_REG_CUM', None, False, ['1', '2', '9'])
		
		self.colunas.add_coluna(self.COD_INC_TRIB)
		self.colunas.add_coluna(self.IND_APRO_CRED)
		self.colunas.add_coluna(self.COD_TIPO_CONT)
		self.colunas.add_coluna(self.IND_REG_CUM)
		
		self.registros_0111 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_0111)
		
	
	def add_r0111(self, registro_0111):
		self.registros_0111.add_registro(registro_0111)
 
# REGISTRO 0111: TABELA DE RECEITA BRUTA MENSAL PARA FINS DE RATEIO DE CRÉDITOS COMUNS
class registro_0111(registro_efd_contribuicoes_base):
	
	def __init__(self):
		super(registro_0111, self).__init__('0111')
		self.REC_BRU_NCUM_TRIB_MI = coluna_efd_contribuicoes_base('REC_BRU_NCUM_TRIB_MI', 0.0, True)
		self.REC_BRU_NCUM_NT_MI   = coluna_efd_contribuicoes_base('REC_BRU_ NCUM_NT_MI', 0.0, True)
		self.REC_BRU_NCUM_EXP     = coluna_efd_contribuicoes_base('REC_BRU_ NCUM_EXP', 0.0, True)
		self.REC_BRU_CUM          = coluna_efd_contribuicoes_base('REC_BRU_CUM', 0.0, True)
		self.REC_BRU_TOTAL        = coluna_efd_contribuicoes_base('REC_BRU_TOTAL', 0.0, True)

		self.colunas.add_coluna(self.REC_BRU_NCUM_TRIB_MI)
		self.colunas.add_coluna(self.REC_BRU_NCUM_NT_MI)
		self.colunas.add_coluna(self.REC_BRU_NCUM_EXP)
		self.colunas.add_coluna(self.REC_BRU_CUM)
		self.colunas.add_coluna(self.REC_BRU_TOTAL)


# REGISTRO 0120: IDENTIFICAÇÃO DE PERÍODOS DISPENSADOS DA ESCRITURAÇÃO FISCAL DIGITAL DAS CONTRIBUIÇÕES – EFD-CONTRIBUIÇÕES
class registro_0120(registro_efd_contribuicoes_base):
	
	def __init__(self):
		super(registro_0120, self).__init__('0120')
		self.MES_DISPENSA = coluna_efd_contribuicoes_base('MES_DISPENSA', None, True)
		self.INF_COMP     = coluna_efd_contribuicoes_base('INF_COMP', None)
		
		self.colunas.add_coluna(self.MES_DISPENSA)
		self.colunas.add_coluna(self.INF_COMP)


# REGISTRO 0140: TABELA DE CADASTRO DE ESTABELECIMENTO
class registro_0140(registro_efd_contribuicoes_base):
	
	def __init__(self):
		super(registro_0140, self).__init__('0140')

		self.COD_EST = coluna_efd_contribuicoes_base('COD_EST', None, True)
		self.NOME    = coluna_efd_contribuicoes_base('NOME', None, True)
		self.CNPJ    = coluna_efd_contribuicoes_base('CNPJ', None, True)
		self.UF      = coluna_efd_contribuicoes_base('UF', None, True)
		self.IE      = coluna_efd_contribuicoes_base('IE', None, False)
		self.COD_MUN = coluna_efd_contribuicoes_base('COD_MUN', None, True)
		self.IM      = coluna_efd_contribuicoes_base('IM', None, False)
		self.SUFRAMA = coluna_efd_contribuicoes_base('SUFRAMA', None, False)

		self.colunas.add_coluna(self.COD_EST)
		self.colunas.add_coluna(self.NOME)
		self.colunas.add_coluna(self.CNPJ)
		self.colunas.add_coluna(self.UF)
		self.colunas.add_coluna(self.IE)
		self.colunas.add_coluna(self.COD_MUN)
		self.colunas.add_coluna(self.IM)
		self.colunas.add_coluna(self.SUFRAMA)
		
		# Registros 0145
		self.registros_0145 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0145)
		
		# Registros 0150
		self.registros_0150 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0150)

		# Registros 0190
		self.registros_0190 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0190)

		# Registros 0200
		self.registros_0200 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0200)

		# Registros 0400
		self.registros_0400 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0400)

		# Registros 0450
		self.registros_0450 = registros_efd_contribuicoes_base(gera_indice=True)
		self.count_0450 = 0
		self.componentes.add(self.registros_0450)
		
	def check_r0145(self, registro_0145):
		
		# Registro ja existe, retorna False		
		if not self.registros_0145.check_registro(registro_0145):
			
			r0145 = self.registros_0145.registro_por_chave(registro_0145.chave())
			
			if r0145 != None:
				r0145.VL_REC_TOT.valor += registro_0145.VL_REC_TOT.valor
				r0145.VL_REC_ATIV.valor += registro_0145.VL_REC_ATIV.valor
				r0145.VL_REC_DEMAIS_ATIV.valor += registro_0145.VL_REC_DEMAIS_ATIV.valor
	
	def check_r0150(self, registro_0150):
		return self.registros_0150.check_registro(registro_0150)

	def check_r0190(self, registro_0190):
		return self.registros_0190.check_registro(registro_0190)

	def check_r0200(self, registro_0200):
		return self.registros_0200.check_registro(registro_0200)

	def check_r0400(self, registro_0400):
		self.registros_0400.check_registro(registro_0400)
	
	def check_r0450(self, registro_0450):

		if not self.registros_0450.check_registro(registro_0450):
			
			r0450 = self.registros_0450.registro_por_chave(registro_0450.chave())
			
			if r0450 != None:
				
				return r0450.COD_INF.valor
			
			else:
				
				return 0
		else:
			
			self.count_0450 += 1
			
			registro_0450.COD_INF.valor = self.count_0450
			
			return self.count_0450
	
		
	def chave(self):
		
		return str(self.COD_EST.valor)
		
# REGISTRO 0145: REGIME DE APURAÇÃO DA CONTRIBUIÇÃO PREVIDENCIÁRIA SOBRE A RECEITA BRUTA
class registro_0145(registro_efd_contribuicoes_base):
	
	def __init__(self):
		super(registro_0145, self).__init__('0145')

		self.COD_INC_TRIB = coluna_efd_contribuicoes_base('COD_INC_TRIB', None, True, ['1', '2'] ) 
		self.VL_REC_TOT = coluna_efd_contribuicoes_base('VL_REC_TOT', 0.0, True)
		self.VL_REC_ATIV = coluna_efd_contribuicoes_base('VL_REC_ATIV', 0.0, True) 
		self.VL_REC_DEMAIS_ATIV = coluna_efd_contribuicoes_base('VL_REC_DEMAIS_ATIV', 0.0)
		self.INFO_COMPL = coluna_efd_contribuicoes_base('INFO_COMPL', None)
		
		self.colunas.add_coluna(self.COD_INC_TRIB)
		self.colunas.add_coluna(self.VL_REC_TOT)
		self.colunas.add_coluna(self.VL_REC_ATIV)
		self.colunas.add_coluna(self.VL_REC_DEMAIS_ATIV)
		self.colunas.add_coluna(self.INFO_COMPL)						

# REGISTRO 0150: TABELA DE CADASTRO DO PARTICIPANTE
class registro_0150(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0150, self).__init__('0150')
		
		self.COD_PART = coluna_efd_contribuicoes_base('COD_PART', None, True)
		self.NOME = coluna_efd_contribuicoes_base('NOME', None, True)
		self.COD_PAIS = coluna_efd_contribuicoes_base('COD_PAIS', None, True)
		self.CNPJ = coluna_efd_contribuicoes_base('CNPJ', None)
		self.CPF = coluna_efd_contribuicoes_base('CPF', None)
		self.IE = coluna_efd_contribuicoes_base('IE', None)
		self.COD_MUN = coluna_efd_contribuicoes_base('COD_MUN', None)
		self.SUFRAMA = coluna_efd_contribuicoes_base('SUFRAMA', None)
		self.END = coluna_efd_contribuicoes_base('END', None)
		self.NUM = coluna_efd_contribuicoes_base('NUM', None)
		self.COMPL = coluna_efd_contribuicoes_base('COMPL', None)
		self.BAIRRO = coluna_efd_contribuicoes_base('BAIRRO', None)

		self.colunas.add_coluna(self.COD_PART)
		self.colunas.add_coluna(self.NOME)
		self.colunas.add_coluna(self.COD_PAIS)
		self.colunas.add_coluna(self.CNPJ)
		self.colunas.add_coluna(self.CPF)
		self.colunas.add_coluna(self.IE)
		self.colunas.add_coluna(self.COD_MUN)
		self.colunas.add_coluna(self.SUFRAMA)
		self.colunas.add_coluna(self.END)
		self.colunas.add_coluna(self.NUM)
		self.colunas.add_coluna(self.COMPL)
		self.colunas.add_coluna(self.BAIRRO)
	
	def chave(self):
		
		return str(self.COD_PART.valor)


#REGISTRO 0190: IDENTIFICAÇÃO DAS UNIDADES DE MEDIDA
class registro_0190(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0190, self).__init__('0190')
		
		self.UNID = coluna_efd_contribuicoes_base('UNID', None, True)
		self.DESCR = coluna_efd_contribuicoes_base('DESCR', None, True)
		
		self.colunas.add_coluna(self.UNID)
		self.colunas.add_coluna(self.DESCR)

	def chave(self):
		
		return str(self.UNID.valor)

# REGISTRO 0200: TABELA DE IDENTIFICAÇÃO DO ITEM (PRODUTOS E SERVIÇOS)
class registro_0200(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0200, self).__init__('0200')
		
		self.COD_ITEM = coluna_efd_contribuicoes_base('COD_ITEM', None, True)
		self.DESCR_ITEM = coluna_efd_contribuicoes_base('DESCR_ITEM', None, True)
		self.COD_BARRA = coluna_efd_contribuicoes_base('COD_BARRA', None)
		self.COD_ANT_ITEM = coluna_efd_contribuicoes_base('COD_ANT_ITEM', None)
		self.UNID_INV = coluna_efd_contribuicoes_base('UNID_INV', None)
		self.TIPO_ITEM = coluna_efd_contribuicoes_base('TIPO_ITEM', None, True, ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '99'])
		self.COD_NCM = coluna_efd_contribuicoes_base('COD_NCM', None)
		self.EX_IPI = coluna_efd_contribuicoes_base('EX_IPI', None)
		self.COD_GEN = coluna_efd_contribuicoes_base('COD_GEN', None)
		self.COD_LST = coluna_efd_contribuicoes_base('COD_LST', None)
		self.ALIQ_ICMS = coluna_efd_contribuicoes_base('ALIQ_ICMS', None)

		self.colunas.add_coluna(self.COD_ITEM)
		self.colunas.add_coluna(self.DESCR_ITEM)
		self.colunas.add_coluna(self.COD_BARRA)
		self.colunas.add_coluna(self.COD_ANT_ITEM)
		self.colunas.add_coluna(self.UNID_INV)
		self.colunas.add_coluna(self.TIPO_ITEM)
		self.colunas.add_coluna(self.COD_NCM)
		self.colunas.add_coluna(self.EX_IPI)
		self.colunas.add_coluna(self.COD_GEN)
		self.colunas.add_coluna(self.COD_LST)
		self.colunas.add_coluna(self.ALIQ_ICMS)
		
	def chave(self):
		
		return str(self.COD_ITEM.valor)


# TODO: REGISTRO 0205: ALTERAÇÃO DO ITEM
# TODO: REGISTRO 0206: CÓDIGO DE PRODUTO CONFORME TABELA ANP (COMBUSTÍVEIS)
# TODO: REGISTRO 0208: CÓDIGO DE GRUPOS POR MARCA COMERCIAL – REFRI (BEBIDAS FRIAS).

# REGISTRO 0400: TABELA DE NATUREZA DA OPERAÇÃO/PRESTAÇÃO
class registro_0400(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0400, self).__init__('0400')
		
		self.COD_NAT = coluna_efd_contribuicoes_base('COD_NAT', None, True)
		self.DESCR_NAT = coluna_efd_contribuicoes_base('DESCR_NAT', None, True)
		
		self.colunas.add_coluna(self.COD_NAT)
		self.colunas.add_coluna(self.DESCR_NAT)
		
		
	def chave(self):
		
		return str(self.COD_NAT.valor)

# REGISTRO 0450: TABELA DE INFORMAÇÃO COMPLEMENTAR DO DOCUMENTO FISCAL
class registro_0450(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0450, self).__init__('0450')

		self.COD_INF = coluna_efd_contribuicoes_base('COD_INF', None, True)
		self.TXT = coluna_efd_contribuicoes_base('TXT', None, True)
		
		self.colunas.add_coluna(self.COD_INF)
		self.colunas.add_coluna(self.TXT)

	def chave(self):
		
		return str(self.TXT.valor)

# REGISTRO 0500: PLANO DE CONTAS CONTÁBEIS
class registro_0500(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0500, self).__init__('0500')
		
		self.DT_ALT = coluna_efd_contribuicoes_base('DT_ALT', '00000000', True)
		self.COD_NAT_CC = coluna_efd_contribuicoes_base('COD_NAT_CC', None, True, ['01', '02', '03', '04', '05', '09'])
		self.IND_CTA = coluna_efd_contribuicoes_base('IND_CTA', None, True, ['S', 'A'])
		self.NIVEL = coluna_efd_contribuicoes_base('NÍVEL', 0, True)
		self.COD_CTA = coluna_efd_contribuicoes_base('COD_CTA', None, True)
		self.NOME_CTA = coluna_efd_contribuicoes_base('NOME_CTA', None, True)
		self.COD_CTA_REF = coluna_efd_contribuicoes_base('COD_CTA_REF', None)
		self.CNPJ_EST = coluna_efd_contribuicoes_base('CNPJ_EST', None)
		 
		self.colunas.add_coluna(self.DT_ALT)
		self.colunas.add_coluna(self.COD_NAT_CC)
		self.colunas.add_coluna(self.IND_CTA)
		self.colunas.add_coluna(self.NIVEL)
		self.colunas.add_coluna(self.COD_CTA)
		self.colunas.add_coluna(self.NOME_CTA)
		self.colunas.add_coluna(self.COD_CTA_REF)
		self.colunas.add_coluna(self.CNPJ_EST)

	def chave(self):
		
		return str(self.COD_CTA.valor)
 
 
# REGISTRO 0600: CENTRO DE CUSTOS
class registro_0600(registro_efd_contribuicoes_base):

	def __init__(self):
		super(registro_0600, self).__init__('0600')

		self.DT_ALT = coluna_efd_contribuicoes_base('DT_ALT', '00000000', True)
 		self.COD_CCUS = coluna_efd_contribuicoes_base('COD_CCUS', None, True)
 		self.CCUS = coluna_efd_contribuicoes_base('CCUS', None, True) 

		self.colunas.add_coluna(self.DT_ALT)
		self.colunas.add_coluna(self.COD_CCUS)
		self.colunas.add_coluna(self.CCUS)
  
 	def chave(self):
		
		return str(self.COD_CCUS.valor)

														
# REGISTRO A010: IDENTIFICAÇÃO DO ESTABELECIMENTO
class registro_A010(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_A010, self).__init__('A010')
		
		self.CNPJ = coluna_efd_contribuicoes_base('CNPJ', None, True)
		
		self.colunas.add_coluna(self.CNPJ)

		self.registros_A100 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_A100)
		

 	def chave(self):
		
		return str(self.CNPJ.valor)
	
	def check_A100(self, registro_A100):
		
		# Registro ja existe, retorna False		
		if not self.registros_A100.check_registro(registro_A100):
			
			rA100 = self.registros_A100.registro_por_chave(registro_A100.chave())
			
			if rA100 != None:
				
				rA100.VL_DOC.valor += registro_A100.VL_DOC.valor
				rA100.VL_DESC.valor += registro_A100.VL_DESC.valor
				rA100.VL_BC_PIS.valor += registro_A100.VL_BC_PIS.valor
				rA100.VL_PIS.valor += registro_A100.VL_PIS.valor
				rA100.VL_BC_COFINS.valor += registro_A100.VL_BC_COFINS.valor
				rA100.VL_COFINS.valor += registro_A100.VL_COFINS.valor
				rA100.VL_PIS_RET.valor += registro_A100.VL_PIS_RET.valor
				rA100.VL_COFINS_RET.valor += registro_A100.VL_COFINS_RET.valor
				rA100.VL_ISS.valor += registro_A100.VL_ISS.valor
						
				# Adiciona registros A170
				for rA170 in registro_A100.registros_A170:
					rA100.registros_A170.add_registro(rA170)
				

				
# REGISTRO A100: DOCUMENTO - NOTA FISCAL DE SERVIÇO
class registro_A100(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_A100, self).__init__('A100')
		
		self.IND_OPER = coluna_efd_contribuicoes_base('IND_OPER', None, True, ['0', '1'])
		self.IND_EMIT = coluna_efd_contribuicoes_base('IND_EMIT', None, True, ['0', '1'])
		self.COD_PART = coluna_efd_contribuicoes_base('COD_PART', None)
		self.COD_SIT  = coluna_efd_contribuicoes_base('COD_SIT', None, True, ['00', '02'])
		self.SER = coluna_efd_contribuicoes_base('SER', None)
		self.SUB = coluna_efd_contribuicoes_base('SUB', None)
		self.NUM_DOC = coluna_efd_contribuicoes_base('NUM_DOC', None, True)
		self.CHV_NFSE = coluna_efd_contribuicoes_base('CHV_NFSE', None)
		self.DT_DOC = coluna_efd_contribuicoes_base('DT_DOC', None, True)
		self.DT_EXE_SERV = coluna_efd_contribuicoes_base('DT_EXE_SERV', None)
		self.VL_DOC = coluna_efd_contribuicoes_base('VL_DOC', 0.0, True)
		self.IND_PGTO = coluna_efd_contribuicoes_base('IND_PGTO', None, True, ['0', '1', '9'])
		self.VL_DESC = coluna_efd_contribuicoes_base('VL_DESC', 0.0)
		self.VL_BC_PIS = coluna_efd_contribuicoes_base('VL_BC_PIS', 0.0, True)
		self.VL_PIS = coluna_efd_contribuicoes_base('VL_PIS', 0.0, True)
		self.VL_BC_COFINS = coluna_efd_contribuicoes_base('VL_BC_COFINS', 0.0, True)
		self.VL_COFINS = coluna_efd_contribuicoes_base('VL_COFINS', 0.0, True)
		self.VL_PIS_RET = coluna_efd_contribuicoes_base('VL_PIS_RET', 0.0)
		self.VL_COFINS_RET = coluna_efd_contribuicoes_base('VL_COFINS_RET', 0.0)
		self.VL_ISS = coluna_efd_contribuicoes_base('VL_ISS', 0.0)


		self.colunas.add_coluna(self.IND_OPER)
		self.colunas.add_coluna(self.IND_EMIT)
		self.colunas.add_coluna(self.COD_PART)
		self.colunas.add_coluna(self.COD_SIT)
		self.colunas.add_coluna(self.SER)
		self.colunas.add_coluna(self.SUB)
		self.colunas.add_coluna(self.NUM_DOC)
		self.colunas.add_coluna(self.CHV_NFSE)
		self.colunas.add_coluna(self.DT_DOC)
		self.colunas.add_coluna(self.DT_EXE_SERV)
		self.colunas.add_coluna(self.VL_DOC)
		self.colunas.add_coluna(self.IND_PGTO)
		self.colunas.add_coluna(self.VL_DESC)
		self.colunas.add_coluna(self.VL_BC_PIS)
		self.colunas.add_coluna(self.VL_PIS)
		self.colunas.add_coluna(self.VL_BC_COFINS)
		self.colunas.add_coluna(self.VL_COFINS)
		self.colunas.add_coluna(self.VL_PIS_RET)
		self.colunas.add_coluna(self.VL_COFINS_RET)
		self.colunas.add_coluna(self.VL_ISS)
				
		# Registros A110
		self.registros_A110 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_A110)
		
		# Registros A111
		self.registros_A111 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_A111)

		# Registros A120
		self.registros_A120 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_A120)

		# Registros A170		
		self.registros_A170 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_A170)

	def add_rA110(self, registro_A110):
		
		self.registros_A110.add_registro(registro_A110)

	def add_rA111(self, registro_A111):
		
		self.registros_A111.add_registro(registro_A111)

	def add_rA120(self, registro_A120):
		
		self.registros_A120.add_registro(registro_A120)
	
	def add_rA170(self, registro_A170):
		
		self.registros_A170.add_registro(registro_A170)

	def chave(self):
		
		return str(self.IND_OPER.valor) + '-' + \
	           str(self.COD_PART.valor) + '-' + \
	           str(self.NUM_DOC.valor) + '-' + \
	           str(self.SER.valor) + '-' + \
	           str(self.SUB.valor) + '-' + \
	           str(self.DT_DOC.valor)

# REGISTRO A110: COMPLEMENTO DO DOCUMENTO - INFORMAÇÃO COMPLEMENTAR DA NF
class registro_A110(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_A110, self).__init__('A110')
		self.COD_INF = coluna_efd_contribuicoes_base('COD_INF', None, True)
		self.TXT_COMPL = coluna_efd_contribuicoes_base('TXT_COMPL', None)
		
		self.colunas.add_coluna(self.COD_INF)
		self.colunas.add_coluna(self.TXT_COMPL)


# REGISTRO A111: PROCESSO REFERENCIADO
class registro_A111(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_A111, self).__init__('A111')
		
		self.NUM_PROC = coluna_efd_contribuicoes_base('NUM_PROC', None, True)
		self.IND_PROC = coluna_efd_contribuicoes_base('IND_PROC', None, True, ['1', '3', '9'])
		
		self.colunas.add_coluna(self.NUM_PROC)
		self.colunas.add_coluna(self.IND_PROC)

# REGISTRO A120: INFORMAÇÃO COMPLEMENTAR - OPERAÇÕES DE IMPORTAÇÃO
class registro_A120(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_A120, self).__init__('A120')

		self.VL_TOT_SERV = coluna_efd_contribuicoes_base('VL_TOT_SERV', None, True)
		self.VL_BC_PIS = coluna_efd_contribuicoes_base('VL_BC_PIS', None, True)
		self.VL_PIS_IMP = coluna_efd_contribuicoes_base('VL_PIS_IMP', None)
		self.DT_PAG_PIS = coluna_efd_contribuicoes_base('DT_PAG_PIS', None)
		self.VL_BC_COFINS = coluna_efd_contribuicoes_base('VL_BC_COFINS', None, True)
		self.VL_COFINS_IMP = coluna_efd_contribuicoes_base('VL_COFINS_IMP', None)
		self.DT_PAG_COFINS = coluna_efd_contribuicoes_base('DT_PAG_COFINS', None)
		self.LOC_EXE_SERV = coluna_efd_contribuicoes_base('LOC_EXE_SERV', None, True, ['0', '1'])
		
		self.colunas.add_coluna(self.VL_TOT_SERV)
		self.colunas.add_coluna(self.VL_BC_PIS)
		self.colunas.add_coluna(self.VL_PIS_IMP)
		self.colunas.add_coluna(self.DT_PAG_PIS)
		self.colunas.add_coluna(self.VL_BC_COFINS)
		self.colunas.add_coluna(self.VL_COFINS_IMP)
		self.colunas.add_coluna(self.DT_PAG_COFINS)
		self.colunas.add_coluna(self.LOC_EXE_SERV)

# REGISTRO A170: COMPLEMENTO DO DOCUMENTO - ITENS DO DOCUMENTO
class registro_A170(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_A170, self).__init__('A170')

		self.NUM_ITEM = coluna_efd_contribuicoes_base('NUM_ITEM', None, True)
		self.COD_ITEM = coluna_efd_contribuicoes_base('COD_ITEM', None, True)
		self.DESCR_COMPL = coluna_efd_contribuicoes_base('DESCR_COMPL', None)
		self.VL_ITEM = coluna_efd_contribuicoes_base('VL_ITEM', None, True)
		self.VL_DESC = coluna_efd_contribuicoes_base('VL_DESC', None)
		self.NAT_BC_CRED = coluna_efd_contribuicoes_base('NAT_BC_CRED', None)
		self.IND_ORIG_CRED = coluna_efd_contribuicoes_base('IND_ORIG_CRED', None)
		self.CST_PIS = coluna_efd_contribuicoes_base('CST_PIS', None, True)
		self.VL_BC_PIS = coluna_efd_contribuicoes_base('VL_BC_PIS', None)
		self.ALIQ_PIS = coluna_efd_contribuicoes_base('ALIQ_PIS', None)
		self.VL_PIS = coluna_efd_contribuicoes_base('VL_PIS', None)
		self.CST_COFINS = coluna_efd_contribuicoes_base('CST_COFINS', None, True)
		self.VL_BC_COFINS = coluna_efd_contribuicoes_base('VL_BC_COFINS', None)
		self.ALIQ_COFINS = coluna_efd_contribuicoes_base('ALIQ_COFINS', None)
		self.VL_COFINS = coluna_efd_contribuicoes_base('VL_COFINS', None)
		self.COD_CTA = coluna_efd_contribuicoes_base('COD_CTA', None)
		self.COD_CCUS = coluna_efd_contribuicoes_base('COD_CCUS', None)

		self.colunas.add_coluna(self.NUM_ITEM)
		self.colunas.add_coluna(self.COD_ITEM)
		self.colunas.add_coluna(self.DESCR_COMPL)
		self.colunas.add_coluna(self.VL_ITEM)
		self.colunas.add_coluna(self.VL_DESC)
		self.colunas.add_coluna(self.NAT_BC_CRED)
		self.colunas.add_coluna(self.IND_ORIG_CRED)
		self.colunas.add_coluna(self.CST_PIS)
		self.colunas.add_coluna(self.VL_BC_PIS)
		self.colunas.add_coluna(self.ALIQ_PIS)
		self.colunas.add_coluna(self.VL_PIS)
		self.colunas.add_coluna(self.CST_COFINS)
		self.colunas.add_coluna(self.VL_BC_COFINS)
		self.colunas.add_coluna(self.ALIQ_COFINS)
		self.colunas.add_coluna(self.VL_COFINS)
		self.colunas.add_coluna(self.COD_CTA)
		self.colunas.add_coluna(self.COD_CCUS)

class registro_A100_facil(object):

	def __init__(self):
		
		self.COD_EST = coluna_efd_contribuicoes_base('COD_EST', None, True) 
		self.registro_A100 = registro_A100()
		self.registros_A110_facil = registros_efd_contribuicoes_base()
	
	def add_rA110_facil(self, registro_A110_facil):
		self.registros_A110_facil.add_registro(registro_A110_facil)

class registro_A110_facil(registro_efd_contribuicoes_base):
	
	def __init__(self):

		self.TXT = coluna_efd_contribuicoes_base('TXT', None, True)
		self.TXT_COMPL = coluna_efd_contribuicoes_base('TXT_COMPL', None)

# REGISTRO C010: IDENTIFICAÇÃO DO ESTABELECIMENTO
class registro_C010(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C010, self).__init__('C010')
		
		self.CNPJ = coluna_efd_contribuicoes_base('CNPJ', None, True)
		self.IND_ESCRI = coluna_efd_contribuicoes_base('IND_ESCRI', None)

		self.colunas.add_coluna(self.CNPJ)
		self.colunas.add_coluna(self.IND_ESCRI)

		self.registros_C100 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C100)
		
		self.registros_C180 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C180)

		self.registros_C190 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C190)
		
 	def chave(self):
		
		return str(self.CNPJ.valor)

	def check_C100(self, registro_C100):
		
		# Registro ja existe, retorna False		
		if not self.registros_C100.check_registro(registro_C100):
			
			rC100 = self.registros_C100.registro_por_chave(registro_C100.chave())
			
			if rC100 != None:
				
				rC100.VL_DOC.valor += registro_C100.VL_DOC.valor
				rC100.VL_DESC.valor += registro_C100.VL_DESC.valor
				rC100.VL_FRT.valor += registro_C100.VL_FRT.valor
				rC100.VL_SEG.valor += registro_C100.VL_SEG.valor
				rC100.VL_OUT_DA.valor += registro_C100.VL_OUT_DA.valor
				rC100.VL_BC_ICMS.valor += registro_C100.VL_BC_ICMS.valor
				rC100.VL_ICMS.valor += registro_C100.VL_ICMS.valor
				rC100.VL_BC_ICMS_ST.valor += registro_C100.VL_BC_ICMS_ST.valor
				rC100.VL_IPI.valor += registro_C100.VL_IPI.valor
				rC100.VL_PIS.valor += registro_C100.VL_PIS.valor
				rC100.VL_COFINS.valor += registro_C100.VL_COFINS.valor
				rC100.VL_PIS_ST.valor += registro_C100.VL_PIS_ST.valor
				rC100.VL_COFINS_ST.valor += registro_C100.VL_COFINS_ST.valor
						
				# Adiciona registros A170
				for rC170 in registro_C100.registros_C170:
					rC100.registros_C170.add_registro(rC170)

	def check_C180(self, registro_C180):

		# Registro ja existe, retorna False		
		if not self.registros_C180.check_registro(registro_C180):
			
			rC180 = self.registros_C180.registro_por_chave(registro_C180.chave())
			
			if rC180 != None:
				
				rC180.VL_TOT_ITEM.valor += registro_C180.VL_TOT_ITEM.valor
				
				for rC181 in registro_C180.registros_C181:
					rC180.check_C181(rC181)
					
				for rC185 in registro_C180.registros_C185:
					rC180.check_C185(rC185)

	def check_C190(self, registro_C190):

		# Registro ja existe, retorna False		
		if not self.registros_C190.check_registro(registro_C190):
			
			rC190 = self.registros_C190.registro_por_chave(registro_C190.chave())
			
			if rC190 != None:
				
				rC190.VL_TOT_ITEM.valor += registro_C190.VL_TOT_ITEM.valor
				
				for rC191 in registro_C190.registros_C191:
					rC190.check_C191(rC191)
					
				for rC195 in registro_C190.registros_C195:
					rC190.check_C195(rC195)		
					
# REGISTRO C100: DOCUMENTO - NOTA FISCAL (CÓDIGO 01), NOTA FISCAL AVULSA
# (CÓDIGO 1B), NOTA FISCAL DE PRODUTOR (CÓDIGO 04) e NF-e (CÓDIGO 55)
class registro_C100(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C100, self).__init__('C100')
	
		self.IND_OPER = coluna_efd_contribuicoes_base('IND_OPER', None, True, ['0', '1'])
		self.IND_EMIT = coluna_efd_contribuicoes_base('IND_EMIT', None, True, ['0', '1'])
		self.COD_PART = coluna_efd_contribuicoes_base('COD_PART', None, True)
		self.COD_MOD = coluna_efd_contribuicoes_base('COD_MOD', None, True, ['01', '1B', '04', '55'])
		self.COD_SIT = coluna_efd_contribuicoes_base('COD_SIT', None, True, ['00', '01', '02', '03', '04', '05', '06', '07', '08'])
		self.SER = coluna_efd_contribuicoes_base('SER', None)
		self.NUM_DOC = coluna_efd_contribuicoes_base('NUM_DOC', None, True)
		self.CHV_NFE = coluna_efd_contribuicoes_base('CHV_NFE', None)
		self.DT_DOC = coluna_efd_contribuicoes_base('DT_DOC', None, True)
		self.COD_E_S = coluna_efd_contribuicoes_base('COD_E_S', None)
		self.VL_DOC = coluna_efd_contribuicoes_base('VL_DOC', None, True)
		self.IND_PGTO = coluna_efd_contribuicoes_base('IND_PGTO', None, True, ['0', '1', '2'])
		self.VL_DESC = coluna_efd_contribuicoes_base('VL_DESC', None)
		self.IND_FRT = coluna_efd_contribuicoes_base('IND_FRT', None, True, ['0', '1', '2', '9'])
		self.VL_FRT = coluna_efd_contribuicoes_base('VL_FRT', None)
		self.VL_SEG = coluna_efd_contribuicoes_base('VL_SEG', None)
		self.VL_OUT_DA = coluna_efd_contribuicoes_base('VL_OUT_DA', None)
		self.VL_BC_ICMS = coluna_efd_contribuicoes_base('VL_BC_ICMS', None)
		self.VL_ICMS = coluna_efd_contribuicoes_base('VL_ICMS', None)
		self.VL_BC_ICMS_ST = coluna_efd_contribuicoes_base('VL_BC_ICMS_ST', None)
		self.VL_IPI = coluna_efd_contribuicoes_base('VL_IPI', None)
		self.VL_PIS = coluna_efd_contribuicoes_base('VL_PIS', None)
		self.VL_COFINS = coluna_efd_contribuicoes_base('VL_COFINS', None)
		self.VL_PIS_ST = coluna_efd_contribuicoes_base('VL_PIS_ST', None)
		self.VL_COFINS_ST = coluna_efd_contribuicoes_base('VL_COFINS_ST', None)

		
		self.colunas.add_coluna(self.IND_OPER)
		self.colunas.add_coluna(self.IND_EMIT)
		self.colunas.add_coluna(self.COD_PART)
		self.colunas.add_coluna(self.COD_MOD)
		self.colunas.add_coluna(self.COD_SIT)
		self.colunas.add_coluna(self.SER)
		self.colunas.add_coluna(self.NUM_DOC)
		self.colunas.add_coluna(self.CHV_NFE)
		self.colunas.add_coluna(self.DT_DOC)
		self.colunas.add_coluna(self.COD_E_S)
		self.colunas.add_coluna(self.VL_DOC)
		self.colunas.add_coluna(self.IND_PGTO)
		self.colunas.add_coluna(self.VL_DESC)
		self.colunas.add_coluna(self.IND_FRT)
		self.colunas.add_coluna(self.VL_FRT)
		self.colunas.add_coluna(self.VL_SEG)
		self.colunas.add_coluna(self.VL_OUT_DA)
		self.colunas.add_coluna(self.VL_BC_ICMS)
		self.colunas.add_coluna(self.VL_ICMS)
		self.colunas.add_coluna(self.VL_BC_ICMS_ST)
		self.colunas.add_coluna(self.VL_IPI)
		self.colunas.add_coluna(self.VL_PIS)
		self.colunas.add_coluna(self.VL_COFINS)
		self.colunas.add_coluna(self.VL_PIS_ST)
		self.colunas.add_coluna(self.VL_COFINS_ST)

		# Registros C110
		self.registros_C110 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_C110)
		
		# Registros C111
		self.registros_C111 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_C111)

		# Registros C120
		self.registros_C120 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_C120)

		# Registros C170		
		self.registros_C170 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_C170)


	def add_rC110(self, registro_C110):
		
		self.registros_C110.add_registro(registro_C110)

	def add_rC111(self, registro_C111):
		
		self.registros_C111.add_registro(registro_C111)

	def add_rC120(self, registro_C120):
		
		self.registros_C120.add_registro(registro_C120)
	
	def add_rC170(self, registro_C170):
		
		self.registros_C170.add_registro(registro_C170)

	def chave(self):
		
		return str(self.IND_OPER.valor) + '-' + \
	           str(self.COD_PART.valor) + '-' + \
	           str(self.NUM_DOC.valor) + '-' + \
	           str(self.SER.valor) + '-' + \
	           str(self.DT_DOC.valor)
	           
	           
# REGISTRO C110: COMPLEMENTO DO DOCUMENTO - INFORMAÇÃO COMPLEMENTAR DA NOTA FISCAL (CÓDIGOS 01, 1B, 04 e 55)
class registro_C110(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C110, self).__init__('C110')
		
		self.COD_INF = coluna_efd_contribuicoes_base('COD_INF', None, True)
		self.TXT_COMPL = coluna_efd_contribuicoes_base('TXT_COMPL', None)

		self.colunas.add_coluna(self.COD_INF)
		self.colunas.add_coluna(self.TXT_COMPL)

# REGISTRO C111: PROCESSO REFERENCIADO
class registro_C111(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C111, self).__init__('C111')
		
		self.NUM_PROC = coluna_efd_contribuicoes_base('NUM_PROC', None, True)
		self.IND_PROC = coluna_efd_contribuicoes_base('IND_PROC', None, True, ['1', '3', '9'])
		
		self.colunas.add_coluna(self.NUM_PROC)
		self.colunas.add_coluna(self.IND_PROC)


# REGISTRO C120: COMPLEMENTO IMPORTAÇÃO (CÓDIGO 01) DO  DOCUMENTO - OPERAÇÕES DE IMPORTAÇÃO (CÓDIGO 01)
class registro_C120(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C120, self).__init__('C120')

		self.COD_DOC_IMP = coluna_efd_contribuicoes_base('COD_DOC_IMP', None, True)
		self.NUM_DOC_IMP = coluna_efd_contribuicoes_base('NUM_DOC_IMP', None, True)
		self.VL_PIS_IMP = coluna_efd_contribuicoes_base('VL_PIS_IMP', None)
		self.VL_COFINS_IMP = coluna_efd_contribuicoes_base('VL_COFINS_IMP', None)
		self.NUM_ACDRAW = coluna_efd_contribuicoes_base('NUM_ACDRAW', None)
		
		self.colunas.add_coluna(self.COD_DOC_IMP)
		self.colunas.add_coluna(self.NUM_DOC_IMP)
		self.colunas.add_coluna(self.VL_PIS_IMP)
		self.colunas.add_coluna(self.VL_COFINS_IMP)
		self.colunas.add_coluna(self.NUM_ACDRAW)


# REGISTRO C170: COMPLEMENTO DO DOCUMENTO - ITENS DO DOCUMENTO (CÓDIGOS 01, 1B, 04 e 55)
class registro_C170(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C170, self).__init__('C170')
		
		self.NUM_ITEM = coluna_efd_contribuicoes_base('NUM_ITEM', None, True)
		self.COD_ITEM = coluna_efd_contribuicoes_base('COD_ITEM', None, True)
		self.DESCR_COMPL = coluna_efd_contribuicoes_base('DESCR_COMPL', None)
		self.QTD = coluna_efd_contribuicoes_base('QTD', None)
		self.UNID = coluna_efd_contribuicoes_base('UNID', None)
		self.VL_ITEM = coluna_efd_contribuicoes_base('VL_ITEM', 0.0, True)
		self.VL_DESC = coluna_efd_contribuicoes_base('VL_DESC', None)
		self.IND_MOV = coluna_efd_contribuicoes_base('IND_MOV', None)
		self.CST_ICMS = coluna_efd_contribuicoes_base('CST_ICMS', None)
		self.CFOP = coluna_efd_contribuicoes_base('CFOP', None, True)
		self.COD_NAT = coluna_efd_contribuicoes_base('COD_NAT', None)
		self.VL_BC_ICMS = coluna_efd_contribuicoes_base('VL_BC_ICMS', None)
		self.ALIQ_ICMS = coluna_efd_contribuicoes_base('ALIQ_ICMS', None)
		self.VL_ICMS = coluna_efd_contribuicoes_base('VL_ICMS', None)
		self.VL_BC_ICMS_ST = coluna_efd_contribuicoes_base('VL_BC_ICMS_ST', None)
		self.ALIQ_ST = coluna_efd_contribuicoes_base('ALIQ_ST', None)
		self.VL_ICMS_ST = coluna_efd_contribuicoes_base('VL_ICMS_ST', None)
		self.IND_APUR = coluna_efd_contribuicoes_base('IND_APUR', None)
		self.CST_IPI = coluna_efd_contribuicoes_base('CST_IPI', None)
		self.COD_ENQ = coluna_efd_contribuicoes_base('COD_ENQ', None)
		self.VL_BC_IPI = coluna_efd_contribuicoes_base('VL_BC_IPI', None)
		self.ALIQ_IPI = coluna_efd_contribuicoes_base('ALIQ_IPI', None)
		self.VL_IPI = coluna_efd_contribuicoes_base('VL_IPI', None)
		self.CST_PIS = coluna_efd_contribuicoes_base('CST_PIS', None, True)
		self.VL_BC_PIS = coluna_efd_contribuicoes_base('VL_BC_PIS', None)
		self.ALIQ_PIS = coluna_efd_contribuicoes_base('ALIQ_PIS', None)
		self.QUANT_BC_PIS = coluna_efd_contribuicoes_base('QUANT_BC_PIS', None)
		self.ALIQ_PIS_QUANT = coluna_efd_contribuicoes_base('ALIQ_PIS_QUANT', None)
		self.VL_PIS = coluna_efd_contribuicoes_base('VL_PIS', None)
		self.CST_COFINS = coluna_efd_contribuicoes_base('CST_COFINS', None, True)
		self.VL_BC_COFINS = coluna_efd_contribuicoes_base('VL_BC_COFINS', None)
		self.ALIQ_COFINS = coluna_efd_contribuicoes_base('ALIQ_COFINNS', None)
		self.QUANT_BC_COFINS = coluna_efd_contribuicoes_base('QUANT_BC_COFINS', None)
		self.ALIQ_COFINS_QUANT = coluna_efd_contribuicoes_base('ALIQ_COFINS_QUANT', None)
		self.VL_COFINS = coluna_efd_contribuicoes_base('VL_COFINS', None)
		self.COD_CTA = coluna_efd_contribuicoes_base('COD_CTA', None)

		self.colunas.add_coluna(self.NUM_ITEM)
		self.colunas.add_coluna(self.COD_ITEM)
		self.colunas.add_coluna(self.DESCR_COMPL)
		self.colunas.add_coluna(self.QTD)
		self.colunas.add_coluna(self.UNID)
		self.colunas.add_coluna(self.VL_ITEM)
		self.colunas.add_coluna(self.VL_DESC)
		self.colunas.add_coluna(self.IND_MOV)
		self.colunas.add_coluna(self.CST_ICMS)
		self.colunas.add_coluna(self.CFOP)
		self.colunas.add_coluna(self.COD_NAT)
		self.colunas.add_coluna(self.VL_BC_ICMS)
		self.colunas.add_coluna(self.ALIQ_ICMS)
		self.colunas.add_coluna(self.VL_ICMS)
		self.colunas.add_coluna(self.VL_BC_ICMS_ST)
		self.colunas.add_coluna(self.ALIQ_ST)
		self.colunas.add_coluna(self.VL_ICMS_ST)
		self.colunas.add_coluna(self.IND_APUR)
		self.colunas.add_coluna(self.CST_IPI)
		self.colunas.add_coluna(self.COD_ENQ)
		self.colunas.add_coluna(self.VL_BC_IPI)
		self.colunas.add_coluna(self.ALIQ_IPI)
		self.colunas.add_coluna(self.VL_IPI)
		self.colunas.add_coluna(self.CST_PIS)
		self.colunas.add_coluna(self.VL_BC_PIS)
		self.colunas.add_coluna(self.ALIQ_PIS)
		self.colunas.add_coluna(self.QUANT_BC_PIS)
		self.colunas.add_coluna(self.ALIQ_PIS_QUANT)
		self.colunas.add_coluna(self.VL_PIS)
		self.colunas.add_coluna(self.CST_COFINS)
		self.colunas.add_coluna(self.VL_BC_COFINS)
		self.colunas.add_coluna(self.ALIQ_COFINS)
		self.colunas.add_coluna(self.QUANT_BC_COFINS)
		self.colunas.add_coluna(self.ALIQ_COFINS_QUANT)
		self.colunas.add_coluna(self.VL_COFINS)
		self.colunas.add_coluna(self.COD_CTA)

class registro_C100_facil(object):

	def __init__(self):
		
		self.COD_EST = coluna_efd_contribuicoes_base('COD_EST', None, True) 
		self.registro_C100 = registro_C100()
		self.registros_C110_facil = registros_efd_contribuicoes_base()
	
	def add_rC110_facil(self, registro_C110_facil):
		self.registros_C110_facil.add_registro(registro_C110_facil)

class registro_C110_facil(registro_efd_contribuicoes_base):
	
	def __init__(self):

		self.TXT = coluna_efd_contribuicoes_base('TXT', None, True)
		self.TXT_COMPL = coluna_efd_contribuicoes_base('TXT_COMPL', None)		

# REGISTRO C180: CONSOLIDAÇÃO DE NOTAS FISCAIS ELETRÔNICAS EMITIDAS
#  PELA PESSOA JURÍDICA (CÓDIGO 55) – OPERAÇÕES DE VENDAS
class registro_C180(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C180, self).__init__('C180')

		self.COD_MOD = coluna_efd_contribuicoes_base('COD_MOD', None, True, ['55'])
		self.DT_DOC_INI = coluna_efd_contribuicoes_base('DT_DOC_INI', None, True)
		self.DT_DOC_FIN = coluna_efd_contribuicoes_base('DT_DOC_FIN', None, True)
		self.COD_ITEM = coluna_efd_contribuicoes_base('COD_ITEM', None, True)
		self.COD_NCM = coluna_efd_contribuicoes_base('COD_NCM', None)
		self.EX_IPI = coluna_efd_contribuicoes_base('EX_IPI', None)
		self.VL_TOT_ITEM = coluna_efd_contribuicoes_base('VL_TOT_ITEM', None)

		self.colunas.add_coluna(self.COD_MOD)
		self.colunas.add_coluna(self.DT_DOC_INI)
		self.colunas.add_coluna(self.DT_DOC_FIN)
		self.colunas.add_coluna(self.COD_ITEM)
		self.colunas.add_coluna(self.COD_NCM)
		self.colunas.add_coluna(self.EX_IPI)
		self.colunas.add_coluna(self.VL_TOT_ITEM)
		
		# Registros C181
		self.registros_C181 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C181)

		# Registros C185
		self.registros_C185 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C185)

		# Registros C188
		self.registros_C188 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_C188)

	def check_C181(self, registro_C181):
		
		# Registro ja existe, retorna False		
		if not self.registros_C181.check_registro(registro_C181):
			
			rC181 = self.registros_C181.registro_por_chave(registro_C181.chave())
			
			if rC181 != None:
				
				rC181.VL_ITEM.valor += registro_C181.VL_ITEM.valor

	def check_C185(self, registro_C185):
		
		# Registro ja existe, retorna False		
		if not self.registros_C185.check_registro(registro_C185):
			
			rC185 = self.registros_C185.registro_por_chave(registro_C185.chave())
			
			if rC185 != None:
				
				rC185.VL_ITEM.valor += registro_C185.VL_ITEM.valor
	
	def add_C188(self, registro_C188):
		
		return self.registros_C188.add_registro(registro_C188)
	
	def chave(self):
		
		return str(self.COD_MOD.valor) + str(self.COD_ITEM.valor)

#REGISTRO C181: DETALHAMENTO DA CONSOLIDAÇÃO – OPERAÇÕES DE VENDAS – PIS/PASEP
class registro_C181(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C181, self).__init__('C181')
		
		self.CST_PIS = coluna_efd_contribuicoes_base('CST_PIS', None, True)
		self.CFOP = coluna_efd_contribuicoes_base('CFOP', None, True)
		self.VL_ITEM = coluna_efd_contribuicoes_base('VL_ITEM', 0.0, True)
		
		self.colunas.add_coluna(self.CST_PIS)
		self.colunas.add_coluna(self.CFOP)
		self.colunas.add_coluna(self.VL_ITEM)
	
	def chave(self):
		
		return str(self.CST_PIS.valor) + str(self.CFOP.valor)

# REGISTRO C185: DETALHAMENTO DA CONSOLIDAÇÃO – OPERAÇÕES DE VENDAS – COFINS
class registro_C185(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C185, self).__init__('C185')
		
		self.CST_COFINS = coluna_efd_contribuicoes_base('CST_COFINS', None, True)
		self.CFOP = coluna_efd_contribuicoes_base('CFOP', None, True)
		self.VL_ITEM = coluna_efd_contribuicoes_base('VL_ITEM', 0.0, True)
		
		self.colunas.add_coluna(self.CST_COFINS)
		self.colunas.add_coluna(self.CFOP)
		self.colunas.add_coluna(self.VL_ITEM)
	
	def chave(self):
		
		return str(self.CST_COFINS.valor) + str(self.CFOP.valor)


# REGISTRO C188: PROCESSO REFERENCIADO
class registro_C188(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C188, self).__init__('C188')
		
		self.NUM_PROC = coluna_efd_contribuicoes_base('NUM_PROC', None, True)
		self.IND_PROC = coluna_efd_contribuicoes_base('IND_PROC', None, True, ['1', '3', '9'])
		
		self.colunas.add_coluna(self.NUM_PROC)
		self.colunas.add_coluna(self.IND_PROC)
		
class registro_C180_facil(object):

	def __init__(self):
		
		self.COD_EST = coluna_efd_contribuicoes_base('COD_EST', None, True) 
		self.registro_C180 = registro_C180()


# REGISTRO C190: CONSOLIDAÇÃO DE NOTAS FISCAIS ELETRÔNICAS (CÓDIGO 55) 
#  – OPERAÇÕES DE AQUISIÇÃO COM DIREITO A CRÉDITO, E OPERAÇÕES DE
#    DEVOLUÇÃO DE COMPRAS E VENDAS.
class registro_C190(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C190, self).__init__('C190')

		self.COD_MOD = coluna_efd_contribuicoes_base('COD_MOD', None, True, ['55'])
		self.DT_DOC_INI = coluna_efd_contribuicoes_base('DT_DOC_INI', None, True)
		self.DT_DOC_FIN = coluna_efd_contribuicoes_base('DT_DOC_FIN', None, True)
		self.COD_ITEM = coluna_efd_contribuicoes_base('COD_ITEM', None, True)
		self.COD_NCM = coluna_efd_contribuicoes_base('COD_NCM', None)
		self.EX_IPI = coluna_efd_contribuicoes_base('EX_IPI', None)
		self.VL_TOT_ITEM = coluna_efd_contribuicoes_base('VL_TOT_ITEM', None)

		self.colunas.add_coluna(self.COD_MOD)
		self.colunas.add_coluna(self.DT_DOC_INI)
		self.colunas.add_coluna(self.DT_DOC_FIN)
		self.colunas.add_coluna(self.COD_ITEM)
		self.colunas.add_coluna(self.COD_NCM)
		self.colunas.add_coluna(self.EX_IPI)
		self.colunas.add_coluna(self.VL_TOT_ITEM)
		
		# Registros C191
		self.registros_C191 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C191)

		# Registros C195
		self.registros_C195 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C195)

		# Registros C198
		self.registros_C198 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_C198)
		
	def check_C191(self, registro_C191):
		
		# Registro ja existe, retorna False		
		if not self.registros_C191.check_registro(registro_C191):
			
			rC191 = self.registros_C191.registro_por_chave(registro_C191.chave())
			
			if rC191 != None:
				
				rC191.VL_ITEM.valor += registro_C191.VL_ITEM.valor

	def check_C195(self, registro_C195):
		
		# Registro ja existe, retorna False		
		if not self.registros_C195.check_registro(registro_C195):
			
			rC195 = self.registros_C195.registro_por_chave(registro_C195.chave())
			
			if rC195 != None:
				
				rC195.VL_ITEM.valor += registro_C195.VL_ITEM.valor
		
	def add_C198(self, registro_C198):
		
		return self.registros_C198.add_registro(registro_C198)
		
	def chave(self):
		
		return str(self.COD_MOD.valor) + str(self.COD_ITEM.valor)

# REGISTRO C191: DETALHAMENTO DA CONSOLIDAÇÃO – OPERAÇÕES DE
#   AQUISIÇÃO COM DIREITO A CRÉDITO, E OPERAÇÕES DE DEVOLUÇÃO DE
#   COMPRAS E VENDAS – PIS/PASEP
class registro_C191(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C191, self).__init__('C191')
		
		self.CST_PIS = coluna_efd_contribuicoes_base('CST_PIS', None, True)
		self.CFOP = coluna_efd_contribuicoes_base('CFOP', None, True)
		self.VL_ITEM = coluna_efd_contribuicoes_base('VL_ITEM', 0.0, True)
		
		self.colunas.add_coluna(self.CST_PIS)
		self.colunas.add_coluna(self.CFOP)
		self.colunas.add_coluna(self.VL_ITEM)
	
	def chave(self):
		
		return str(self.CST_PIS.valor) + str(self.CFOP.valor)

#REGISTRO C195: DETALHAMENTO DA CONSOLIDAÇÃO - OPERAÇÕES DE
#  AQUISIÇÃO COM DIREITO A CRÉDITO, E OPERAÇÕES DE DEVOLUÇÃO DE
#  COMPRAS E VENDAS – COFINS
class registro_C195(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C195, self).__init__('C195')
		
		self.CST_COFINS = coluna_efd_contribuicoes_base('CST_COFINS', None, True)
		self.CFOP = coluna_efd_contribuicoes_base('CFOP', None, True)
		self.VL_ITEM = coluna_efd_contribuicoes_base('VL_ITEM', 0.0, True)
		
		self.colunas.add_coluna(self.CST_COFINS)
		self.colunas.add_coluna(self.CFOP)
		self.colunas.add_coluna(self.VL_ITEM)
	
	def chave(self):
		
		return str(self.CST_COFINS.valor) + str(self.CFOP.valor)

# REGISTRO C198: PROCESSO REFERENCIADO
class registro_C198(registro_efd_contribuicoes_base):
	
	def __init__(self):
		
		super(registro_C198, self).__init__('C198')
		
		self.NUM_PROC = coluna_efd_contribuicoes_base('NUM_PROC', None, True)
		self.IND_PROC = coluna_efd_contribuicoes_base('IND_PROC', None, True, ['1', '3', '9'])
		
		self.colunas.add_coluna(self.NUM_PROC)
		self.colunas.add_coluna(self.IND_PROC)
		
		
class registro_C190_facil(object):

	def __init__(self):
		
		self.COD_EST = coluna_efd_contribuicoes_base('COD_EST', None, True) 
		self.registro_C190 = registro_C190()	
# --------------------------------------------------------------------------------------------------#
# ----------------------------------------- BLOCOS -------------------------------------------------#
# --------------------------------------------------------------------------------------------------#

class bloco_0(bloco_efd_contribuicoes_base):

	def __init__(self, r0000=None):
		super(bloco_0, self).__init__('0')

		self.r0000 = r0000
		# Registros 0100
		self.registros_0100 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_0100)
		
		#Registro 0110
		self.registro_0110 = registro_0110()
		self.componentes.add(self.registro_0110)
		
		#Registros 0120
		self.registros_0120 = registros_efd_contribuicoes_base()
		self.componentes.add(self.registros_0120)
		
		#Registros 0140
		self.registros_0140 = registros_efd_contribuicoes_base(gera_indice = True)
		self.componentes.add(self.registros_0140)

		# Registros 0500
		self.registros_0500 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0500)

		# Registros 0600
		self.registros_0600 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_0600)

	def add_r0100(self, registro_0100):
		self.registros_0100.add_registro(registro_0100)
	
	def add_r0120(self, registro_0120):
		self.registros_0120.add_registro(registro_0120)
	
	def check_r0140(self, registro_0140):
		return self.registros_0140.check_registro(registro_0140)

	def check_r0500(self, registro_0500):
		return self.registros_0500.check_registro(registro_0500)
	
	def check_r0600(self, registro_0600):
		return self.registros_0600.check_registro(registro_0600)


class bloco_A(bloco_efd_contribuicoes_base):

	def __init__(self, r0000 = None):
		
		super(bloco_A, self).__init__('A')
		
		self.r0000 = r0000
		self.registros_A010 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_A010)
	
	def check_A010(self, registro_A010):
		
		return self.registros_A010.check_registro(registro_A010)
	
	def posiciona_A010(self, COD_EST):

		if self.r0000 == None:
			raise excecao_entrada_invalida(u'Impossível recuperar o registro 0000')
	
		#Verifica se ja existe a filial em 0145
		r0140 = self.r0000.bloco_0.registros_0140.registro_por_chave(COD_EST)
		
		#Se nao existe retorna excessao
		if r0140 == None:
			raise excecao_entrada_invalida(u"Filial %s não lançada no registro 0140" % (COD_EST))
		
		#Recupera o registro A010 correto 
		rA010 = self.registros_A010.registro_por_chave(r0140.CNPJ.valor)
		
		#Se nao existe registro A010, cria
		if rA010 == None:
			rA010 = registro_A010()
			rA010.CNPJ.valor = r0140.CNPJ.valor
			self.check_A010(rA010)
		
		return [r0140, rA010]
		
	
	def checK_A100_facil(self, registros_A100_facil):
		
		COD_EST_old = ''
		rA010 = None
		
		for registro in registros_A100_facil:
			
			if COD_EST_old != registro.COD_EST.valor:
				
				r0140, rA010 = self.posiciona_A010(registro.COD_EST.valor)
			
			if rA010 == None:
				raise excecao_entrada_invalida(u'Impossível determinar entrada A010')
			
			# Insere os registros A110_facil
			for rA110_facil in registro.registros_A110_facil:
				r0450 = registro_0450()
				rA110 = registro_A110()
				
				r0450.TXT.valor = rA110_facil.TXT.valor
				rA110.TXT_COMPL.valor = rA110_facil.TXT_COMPL.valor
				rA110.COD_INF.valor = r0140.check_r0450(r0450)
				registro.registro_A100.add_rA110(rA110)

			# Insere registro A100
			rA010.check_A100(registro.registro_A100)
			
			
			COD_EST_old = registro.COD_EST.valor
		
		
		
class bloco_C(bloco_efd_contribuicoes_base):
	
	def __init__(self, r0000=None):
		super(bloco_C, self).__init__('C')
		
		self.r0000 = r0000
		self.registros_C010 = registros_efd_contribuicoes_base(gera_indice=True)
		self.componentes.add(self.registros_C010)

	def check_C010(self, registro_C010):
		
		return self.registros_C010.check_registro(registro_C010)
	
	def posiciona_C010(self, COD_EST):

		if self.r0000 == None:
			raise excecao_entrada_invalida(u'Impossível recuperar o registro 0000')
	
		#Verifica se ja existe a filial em 0145
		r0140 = self.r0000.bloco_0.registros_0140.registro_por_chave(COD_EST)
		
		#Se nao existe retorna excessao
		if r0140 == None:
			raise excecao_entrada_invalida(u"Filial %s não lançada no registro 0140" % (COD_EST))
		
		#Recupera o registro A010 correto 
		rC010 = self.registros_C010.registro_por_chave(r0140.CNPJ.valor)
		
		#Se nao existe registro A010, cria
		if rC010 == None:
			rC010 = registro_C010()
			rC010.CNPJ.valor = r0140.CNPJ.valor
			self.check_C010(rC010)
		
		return [r0140, rC010]
	
	def checK_C100_facil(self, registros_C100_facil):
		
		COD_EST_old = ''
		rC010 = None
		
		for registro in registros_C100_facil:
			
			if COD_EST_old != registro.COD_EST.valor:
				
				r0140, rC010 = self.posiciona_C010(registro.COD_EST.valor)
			
			if rC010 == None:
				raise excecao_entrada_invalida(u'Impossível determinar entrada A010')
			
			# Insere os registros A110_facil
			for rC110_facil in registro.registros_C110_facil:
				r0450 = registro_0450()
				rC110 = registro_C110()
				
				r0450.TXT.valor = rC110_facil.TXT.valor
				rC110.TXT_COMPL.valor = rC110_facil.TXT_COMPL.valor
				rC110.COD_INF.valor = r0140.check_r0450(r0450)
				registro.registro_C100.add_rC110(rC110)

			# Insere registro A100
			rC010.check_C100(registro.registro_C100)
			
			
			COD_EST_old = registro.COD_EST.valor

	def checK_C180_facil(self, registros_C180_facil):
		
		COD_EST_old = ''
		rC010 = None
		
		for registro in registros_C180_facil:
			
			if COD_EST_old != registro.COD_EST.valor:
				
				r0140, rC010 = self.posiciona_C010(registro.COD_EST.valor)
			
			if rC010 == None:
				raise excecao_entrada_invalida(u'Impossível determinar entrada A010')
			
			# Insere registro A100
			rC010.check_C180(registro.registro_C180)
			
			COD_EST_old = registro.COD_EST.valor			
			
class bloco_D(bloco_efd_contribuicoes_base):

	def __init__(self, r0000=None):
		super(bloco_D, self).__init__('D')
		
		self.r0000 = r0000

class bloco_F(bloco_efd_contribuicoes_base):
	def __init__(self, r0000=None):
		super(bloco_F, self).__init__('F')
		
		self.r0000 = r0000


class bloco_1(bloco_efd_contribuicoes_base):
	def __init__(self, r0000=None):
		super(bloco_1, self).__init__('1')
		
		self.r0000 = r0000


class bloco_M(bloco_efd_contribuicoes_base):
	def __init__(self, r0000=None):
		super(bloco_M, self).__init__('M')
		self.r0000 = r0000


# --------------------------------------------------------------------------------------------------#
# ----------------------------------------- EFD ----------------------------------------------------#
# --------------------------------------------------------------------------------------------------#

class efd_contribuicoes(efd_contribuicoes_base):
	
	def gera_registro_0000(self):
		
		registro = registro_0000()
		
		return registro
		