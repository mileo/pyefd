# -*- encoding: utf-8 -*-

from pyEFD.base import *
from pyEFD.efd_contribuicoes_base import * 
from sys import stdout
from datetime import date

if __name__ == '__main__':
    
    efd = efd_contribuicoes()
    efd.registro_0000.COD_VER.valor = '003'
    efd.registro_0000.TIPO_ESCRIT.valor = '0'
    efd.registro_0000.IND_SIT_ESP.valor = '0'
    efd.registro_0000.DT_INI.valor = date(2013, 03, 01)
    efd.registro_0000.DT_FIN.valor = date(2013, 03, 31)
    efd.registro_0000.NOME.valor = 'NOME DA EMPRESA'
    efd.registro_0000.CNPJ.valor = '104404820000154'
    efd.registro_0000.UF.valor = 'RS'
    efd.registro_0000.COD_MUN.valor = '4303905'
    efd.registro_0000.IND_NAT_PJ.valor = '00'
    efd.registro_0000.IND_ATIV.valor = '0'
    
    #Contador
    r0100 = registro_0100()
    r0100.NOME.valor = 'FABIO NEGRINI'
    r0100.CPF.valor = '03066508943'
    r0100.CRC.valor = 'PR123456'
    efd.registro_0000.bloco_0.add_r0100(r0100)
    
    #0110
    efd.registro_0000.bloco_0.registro_0110.COD_INC_TRIB.valor = '1'
    efd.registro_0000.bloco_0.registro_0110.IND_APRO_CRED.valor = '2' 

    #0111
    r0111 = registro_0111()
    r0111.REC_BRU_CUM.valor = 1500.00
    efd.registro_0000.bloco_0.registro_0110.add_r0111(r0111)
    
    #0120
    r0120_1 = registro_0120()
    r0120_1.MES_DISPENSA.valor = '012013'
    r0120_1.INF_COMP.valor = u'Nenhum faturamento no mes'
    efd.registro_0000.bloco_0.add_r0120(r0120_1)

    r0120_2 = registro_0120()
    r0120_2.MES_DISPENSA.valor = '022013'
    r0120_2.INF_COMP.valor = u'Nenhum faturamento no mes'
    efd.registro_0000.bloco_0.add_r0120(r0120_2)
    
    #0140
    r0140 = registro_0140()
    r0140.COD_EST.valor = '100'
    r0140.NOME.valor = 'NOME DA MATRIZ'
    r0140.CNPJ.valor = '104404820000154'
    r0140.UF.valor = 'RS'
    r0140.COD_MUN.valor = '4303905'

    efd.registro_0000.bloco_0.check_r0140(r0140)
    
    # Segundo teste, nao deve inserir nada aqui!!!
    efd.registro_0000.bloco_0.check_r0140(r0140)
    
    #0145 - Soma os valores em um soh registro
    r0145 = registro_0145()
    r0145.COD_INC_TRIB.valor = '1'
    r0145.VL_REC_TOT.valor = 10000.0
    r0145.VL_REC_ATIV.valor = 10000.0
    
    r0145b = registro_0145()
    r0145b.COD_INC_TRIB.valor = '1'
    r0145b.VL_REC_TOT.valor = 5000.0
    r0145b.VL_REC_ATIV.valor = 0.0
    
    r0140.check_r0145(r0145)
    r0140.check_r0145(r0145b)
    
    #0150 - Insere somente Parceiro novo
    r0150 = registro_0150()
    r0150.COD_PART.valor = 'PART01'
    r0150.NOME.valor = 'Parceiro 01'
    r0150.COD_PAIS.valor = '01058'
    
    r0140.check_r0150(r0150)
    
    #Este nao deve inserir, pois ja inseriu acima
    r0150b = registro_0150()
    r0150b.COD_PART.valor = 'PART01'
    r0150b.NOME.valor = 'Parceiro 01'
    r0150b.COD_PAIS.valor = '01058'
    
    r0140.check_r0150(r0150b)
    
    #Este deve inserir!
    r0150c = registro_0150()
    r0150c.COD_PART.valor = 'PART02'
    r0150c.NOME.valor = 'Parceiro 02'
    r0150c.COD_PAIS.valor = '01058'
    
    r0140.check_r0150(r0150c)
    
    #0190
    r0190 = registro_0190()
    r0190.UNID.valor = 'KG'
    r0190.DESCR.valor = 'Quilo'
    
    r0140.check_r0190(r0190)
    
    #0200
    r0200 = registro_0200()
    r0200.COD_ITEM.valor = 'MATERIAL01'
    r0200.DESCR_ITEM.valor = 'Material 01'
    r0200.TIPO_ITEM.valor = '00'
    
    r0140.check_r0200(r0200)
    
    #0450
    r0450 = registro_0450()
    r0450.TXT.valor = u'Mensagem 001'
    i = r0140.check_r0450(r0450)
    #print(i)

    r0450b = registro_0450()
    r0450b.TXT.valor = u'Mensagem 002'
    i = r0140.check_r0450(r0450b)
    #print(i)
    
    r0450c = registro_0450()
    r0450c.TXT.valor = u'Mensagem 003'
    i = r0140.check_r0450(r0450c)
    #print(i)

    r0450d = registro_0450()
    r0450d.TXT.valor = u'Mensagem 002'
    i = r0140.check_r0450(r0450d)
    #print(i)
    
    # 0500
    r0500 = registro_0500()
    r0500.DT_ALT.valor = date(2013, 01, 01)
    r0500.COD_NAT_CC.valor = '01'
    r0500.IND_CTA.valor = 'A'
    r0500.COD_CTA.valor = '0010010101'
    r0500.NIVEL.valor = 3
    r0500.NOME_CTA.valor = 'Conta do Ativo'

    efd.registro_0000.bloco_0.check_r0500(r0500)

    # 0600
    r0600 = registro_0600()
    r0600.DT_ALT.valor = date(2013,01,02)
    r0600.COD_CCUS.valor = '0100'
    r0600.CCUS.valor = 'Centro de Custos Exemplo'
    efd.registro_0000.bloco_0.check_r0600(r0600)
    
    rC180_facil = registro_C180_facil()
    rC180_facil.COD_EST.valor = '100'
    rC180_facil.registro_C180.COD_MOD.valor = '55'
    rC180_facil.registro_C180.DT_DOC_INI.valor = date(2013, 03, 01)
    rC180_facil.registro_C180.DT_DOC_FIN.valor = date(2013, 03, 31)
    rC180_facil.registro_C180.COD_ITEM.valor = 'MATERIAL01'
    rC180_facil.registro_C180.VL_TOT_ITEM.valor = 10.00
    
    rC181 = registro_C181()
    rC181.CFOP.valor = '1101'
    rC181.CST_PIS.valor = '01'
    rC181.VL_ITEM.valor = 10.00
    rC180_facil.registro_C180.check_C181(rC181)
    

    rC185 = registro_C185()
    #rC185.CFOP.valor = '1101'
    rC185.CST_COFINS.valor = '01'
    rC185.VL_ITEM.valor = 10.00
    rC180_facil.registro_C180.check_C185(rC185)

    rC180_facil2 = registro_C180_facil()
    rC180_facil2.COD_EST.valor = '100'
    rC180_facil2.registro_C180.COD_MOD.valor = '55'
    rC180_facil2.registro_C180.DT_DOC_INI.valor = date(2013, 03, 01)
    rC180_facil2.registro_C180.DT_DOC_FIN.valor = date(2013, 03, 31)
    rC180_facil2.registro_C180.COD_ITEM.valor = 'MATERIAL01'
    rC180_facil2.registro_C180.VL_TOT_ITEM.valor = 15.00
    
    rC181a = registro_C181()
    rC181a.CFOP.valor = '1101'
    rC181a.CST_PIS.valor = '01'
    rC181a.VL_ITEM.valor = 15.00
    rC180_facil2.registro_C180.check_C181(rC181a)
    
    rC185a = registro_C185()
    rC185a.CFOP.valor = '1101'
    rC185a.CST_COFINS.valor = '01'
    rC185a.VL_ITEM.valor = 15.00
    rC180_facil2.registro_C180.check_C185(rC185a)
        
    efd.registro_0000.bloco_C.checK_C180_facil([rC180_facil, rC180_facil2])

    lista, excecoes = efd.valida_para_arquivo()
    
    arquivo = open('./efd_contribuicoes.txt', 'w')
    
    for linha in lista:    
        arquivo.write(linha)
        arquivo.write("\n")
        
    arquivo = open('./erros_efd_contribuicoes.txt', 'w')
    
    #Nas excecoes, o registro e o campo sao objetos
    #Podendo modificar o seu valor diretamente pelo link da excecao
    #Que as alterações serão refletidas no fluxo
    #Exceto os registros automaticos X001 e X990 e o bloco 9, estes sao 100% automaticos
    for excecao in excecoes:
        arquivo.write(str(excecao))
        arquivo.write("\n")
        
    